import unittest
import time
import random
import string
from selenium import webdriver
import sys

class TestLaravelCases(unittest.TestCase):

    name = 'test_' + str(random.randint(000000, 999999)) + '_vlad'
    email = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(6)) + '@' + \
            ''.join(random.choice(string.ascii_lowercase) for _ in range(4)) + '.com'
    pass1 = 'parolamea'
    pass2 = 'parolamea'

    def setUp(self):
        self.driver = webdriver.Chrome('/Users/vladstangu/_Dev/Azimut/laravel-testing/Utilities/chromedriver')
        self.driver.get('https://laravel.dev-society.com/')

    def tearDown(self):
        self.driver.quit()

    def test_aboutUs(self):
        about = self.driver.find_element_by_id("about_page")
        about.click()
        self.assertIn('about', self.driver.current_url)

    def registerUser(self, username, useremail, pass1, pass2):
        register_button = self.driver.find_element_by_id('register_button')
        register_button.click()
        self.assertIn('register', self.driver.current_url)

        name_box = self.driver.find_element_by_id('name')
        name_box.clear()
        name_box.send_keys(username)

        email_box = self.driver.find_element_by_id('email')
        email_box.clear()
        email_box.send_keys(useremail)

        password_box = self.driver.find_element_by_id('password')
        password_box.clear()
        password_box.send_keys(pass1)

        password_confirm_box = self.driver.find_element_by_id('password-confirm')
        password_confirm_box.clear()
        password_confirm_box.send_keys(pass2)
        time.sleep(2)

        submit_register_btn = self.driver.find_element_by_id('register_account')
        submit_register_btn.click()

    def loginUser(self, email, password):
        login_btn = self.driver.find_element_by_id("login_button")
        login_btn.click()
        self.assertIn('login', self.driver.current_url)
        time.sleep(1)

        email_box = self.driver.find_element_by_id('email')
        email_box.clear()
        email_box.send_keys(email)

        pass_box = self.driver.find_element_by_id("password")
        pass_box.clear()
        pass_box.send_keys(password)

        time.sleep(1)

        submit_btn = self.driver.find_element_by_id("login_user")
        submit_btn.click()

    def test_ValidRegister(self):
        self.registerUser(self.name, self.email, self.pass1, self.pass2)
        self.assertIn('have no posts yet', self.driver.page_source)
        time.sleep(2)

        logout_btn = self.driver.find_element_by_xpath('//*[@id="sidebar-logout"]/a')
        logout_btn.click()
        time.sleep(1)

        self.assertEqual('https://laravel.dev-society.com/', self.driver.current_url)

    def test_InvalidRegister(self):
        #empty username field
        self.registerUser('', self.email, self.pass1, self.pass2)
        self.assertIn('https://laravel.dev-society.com/register', self.driver.current_url)

        #empty email
        self.registerUser(self.name, '', self.pass1, self.pass2)
        self.assertIn('https://laravel.dev-society.com/register', self.driver.current_url)

        #incorrect email
        self.registerUser(self.name, '123@', self.pass1, self.pass2)
        self.assertIn('https://laravel.dev-society.com/register', self.driver.current_url)

        #invalid password
        self.registerUser(self.name, self.email, '1234', '1234')
        self.assertIn('password must be at least 6', self.driver.find_element_by_xpath('//*[@id="app"]/main/div[2]/div/div/div/div[2]/form/div[3]/div/span/strong').text)
        time.sleep(1)

        #password not matching
        self.registerUser(self.name, self.email, self.pass1, '12345678')
        self.assertIn('The password confirmation does not match', self.driver.find_element_by_xpath('//*[@id="app"]/main/div[2]/div/div/div/div[2]/form/div[3]/div/span/strong').text)
        time.sleep(1)

    def test_ValidLogin(self):
        self.loginUser('vulizawo@getnada.com', '12345678')
        self.assertIn('Welcome', self.driver.page_source)
        time.sleep(2)

    def test_InvalidLogin(self):
        pass

    def test_AddPost(self):
        self.test_ValidLogin()

        posts_btn = self.driver.find_element_by_xpath('//*[@id="sidebar-posts"]/a')
        posts_btn.click()
        self.assertIn('posts', self.driver.current_url)
        time.sleep(1)

        create_post = self.driver.find_element_by_id('create_post')
        create_post.click()
        self.assertIn('/posts/create', self.driver.current_url)
        time.sleep(2)

        title = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/input[2]')
        title.clear()
        title.send_keys("Lorem Ipsum")

        content = self.driver.find_element_by_xpath('//*[@id="text_editor"]')
        content.clear()
        content.send_keys('''
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mattis quis ligula vitae pretium. Sed diam diam, aliquam vel vulputate vel, tincidunt ut dolor. Cras posuere, lectus sed tristique ullamcorper, nunc elit hendrerit tortor, in placerat tortor ligula ac tortor. Sed porta ligula lectus. Nunc vitae ultrices elit. Donec dignissim, tellus nec ultricies tincidunt, est velit congue nulla, non pulvinar mauris erat quis orci. Fusce vitae risus lorem. Vivamus imperdiet ligula massa, in aliquam lorem laoreet vel. Maecenas et efficitur dolor. Vestibulum mattis sit amet urna eu consequat. Maecenas ut purus ac turpis malesuada faucibus. Curabitur facilisis nisi dictum tempor cursus. Sed sagittis dui non arcu aliquam sodales.
        ''')
        time.sleep(2)

        submit_btn = self.driver.find_element_by_id('submit_post')
        submit_btn.click()

        postUrl = self.driver.current_url
        list = postUrl.split('/')
        TestLaravelCases.postId = list[-1]

        self.assertIn('Post created', self.driver.page_source)
        time.sleep(2)

    def test_EditPost(self):
        self.test_AddPost()
        gotodashboard = self.driver.find_element_by_id('navbar_dashboard')
        gotodashboard.click()
        self.assertIn('dashboard', self.driver.current_url)

        time.sleep(1)

        selectpostbyId = self.driver.find_element_by_id('edit-post-' + TestLaravelCases.postId)
        selectpostbyId.click()

        time.sleep(2)

        title = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/input[2]')
        title.clear()
        title.send_keys('Limmerick')

        time.sleep(1)

        textbox = self.driver.find_element_by_id('text_editor')
        textbox.clear()
        textbox.send_keys('''There was a man from Darjeeling,

Who boarded a bus bound for Ealing,

It said on the door,

Please don’t spit on the floor,

So he stood up and spat on the ceiling.''')

        time.sleep(2)

        submit = self.driver.find_element_by_id('submit_post')
        submit.click()

        time.sleep(2)

        self.assertIn("Post updated", self.driver.page_source)

    def test_DeletePost(self):
        self.test_AddPost()

        gotodashboard = self.driver.find_element_by_id('navbar_dashboard')
        gotodashboard.click()
        self.assertIn('dashboard', self.driver.current_url)

        time.sleep(1)

        selectpostbyId = self.driver.find_element_by_id('delete-post-' + TestLaravelCases.postId)
        selectpostbyId.click()

        self.assertIn('Post deleted', self.driver.page_source)

    def test_UserToAdmin(self):
        self.loginUser('vulizawo@getnada.com', '12345678')

        settings = self.driver.find_element_by_id('sidebar-settings')
        settings.click()
        self.assertIn('settings', self.driver.current_url)

        edit_account = self.driver.find_element_by_id('edit_account')
        edit_account.click()
        self.assertIn('edit', self.driver.current_url)
        time.sleep(1)

        userrole = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/select')
        userrole.click()
        admin = self.driver.find_element_by_id('admin')
        admin.click()
        time.sleep(1)

        submit = self.driver.find_element_by_id('apply')
        submit.click()
        self.assertIn('User updated', self.driver.page_source)

    def test_AdminToUser(self):
        self.loginUser('vulizawo@getnada.com', '12345678')

        settings = self.driver.find_element_by_id('sidebar-settings')
        settings.click()
        self.assertIn('settings', self.driver.current_url)

        edit_account = self.driver.find_element_by_id('edit_account')
        edit_account.click()
        self.assertIn('edit', self.driver.page_source)
        time.sleep(1)

        userrole = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/select')
        userrole.click()
        admin = self.driver.find_element_by_id('user')
        admin.click()
        time.sleep(1)

        submit = self.driver.find_element_by_id('apply')
        submit.click()
        self.assertIn('User updated', self.driver.page_source)

    def test_EditOtherUser(self):
        self.loginUser("vulizawo@getnada.com", "12345678")
        users = self.driver.find_element_by_id('sidebar-users')
        users.click()
        self.assertIn('admin/users', self.driver.current_url)

        user = self.driver.find_element_by_id('edit-user-4')
        user.click()
        self.assertIn('admin/users/4/edit', self.driver.current_url)

        time.sleep(1)

        name = self.driver.find_element_by_id('edit-name')
        name.clear()
        name.send_keys('edit other user')
        time.sleep(1)

        submit = self.driver.find_element_by_id("apply")
        submit.click()
        self.assertIn('User updated', self.driver.page_source)

    def testDeleteUser(self):
        self.test_ValidRegister()
        self.test_UserToAdmin()

def main(plan):
    test_suite = unittest.TestSuite()

    fisier = open(plan, 'r')
    linii = fisier.read().split('\n')
    for linie in linii:
        test_suite.addTest(TestLaravelCases(linie))
    fisier.close()

    runner = unittest.TextTestRunner(verbosity=3)
    runner.run(test_suite)

main(sys.argv[1])















